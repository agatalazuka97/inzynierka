<?php

namespace App\Http\Controllers;

use App\VendorCategory;
use Illuminate\Http\Request;

class VendorCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        request()->validate([
            'name' => 'required'
        ]);

        $data = array_merge(
          $request->all(),
          ['wedding_id' => auth()->user()->wedding_id]
        );

        VendorCategory::create($data);

        return redirect()->route('vendor.create')
            ->with('success','Kategoria została dodana.');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\VendorCategory  $vendorCategory
     * @return \Illuminate\Http\Response
     */
    public function show(VendorCategory $vendorCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\VendorCategory  $vendorCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(VendorCategory $vendorCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VendorCategory  $vendorCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VendorCategory $vendorCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\VendorCategory  $vendorCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        VendorCategory::find($id)->delete();
        return redirect()->route('vendor.create');
    }
}
