<?php

namespace App\Http\Controllers;

use App\Cost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Charts\CostChart;
use App\Charts\CostActualChart;



class CostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $costs = DB::table('costs')
            ->join('cost_categories', 'cost_category_id', '=', 'cost_categories.id')
            ->where('costs.wedding_id', '=', auth()->user()->wedding_id)
            ->get();

        $costs_info = DB::table('costs')
            ->join('cost_categories', 'cost_category_id', '=', 'cost_categories.id')
            ->select('cost_categories.name', DB::raw('sum(cost_planned) as sum_planned'), DB::raw('sum(cost_actual) as sum_actual'))
            ->where('costs.wedding_id', '=', auth()->user()->wedding_id)
            ->groupBy('name')
            ->orderBy('name')
            ->get();


        if(request()->has('name')){
            $costs = $costs->where('name', request('name'))->groupBy('name');
        } else
            $costs = $costs->groupBy('name');


        $costs_labels = $costs_info->pluck('name')->toArray();
        $costs_planned_data = $costs_info->pluck('sum_planned')->toArray();
        $costs_actual_data = $costs_info->pluck('sum_actual')->toArray();


        $fillColors = [];

        foreach($costs_labels as $label) {
            array_push($fillColors, "rgba(".mt_rand(0, 255).", ".mt_rand(0, 255).", ".mt_rand(0, 255).", 0.2)");
        }


        $chart = new CostChart();
        $chart->labels($costs_labels)
            ->title('Koszty planowane')
            ->minimalist(true)
            ->dataset('My dataset 1', 'doughnut', $costs_planned_data)
            ->backgroundcolor($fillColors);

        $chart_actual = new CostActualChart();
        $chart_actual->labels($costs_labels)
            ->title('Koszty aktualne')
            ->minimalist(true)
            ->dataset('My dataset 1', 'doughnut', $costs_actual_data)
            ->backgroundcolor($fillColors);





        return view('costs.index', compact(['costs', 'costs_info', 'chart', 'chart_actual']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cost  $cost
     * @return \Illuminate\Http\Response
     */
    public function show(Cost $cost)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cost  $cost
     * @return \Illuminate\Http\Response
     */
    public function edit(Cost $cost)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cost  $cost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cost $cost)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cost  $cost
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cost $cost)
    {
        //
    }


}
