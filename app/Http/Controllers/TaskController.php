<?php

namespace App\Http\Controllers;

use App\Task;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $overdue_tasks = Task::orderBy('due_date_planned')
            ->where('wedding_id', '=', auth()->user()->wedding_id)
            ->where('status', '=', '1')
            ->get();


        $tasks = Task::orderBy('due_date_planned')
            ->where('wedding_id','=', auth()->user()->wedding_id)
            ->where('status', '=', '0')
            ->get()
            ->groupBy(function($item) {
                 return Carbon::parse($item->due_date_planned)->format('m-Y');
        });


        return view('tasks.index', compact(['tasks', 'overdue_tasks']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('tasks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        request()->validate([
            'title' => ['required','unique:tasks,title,NULL,id,wedding_id,'.auth()->user()->wedding_id],
            'due_date_planned' => 'required'

        ],
            [
                'title.required' => 'Nazwa zadania jest wymagana.',
                'due_date_planned.required' => 'Wybór daty jest wymagany',
                'title.unique' => 'Istnieje już takie zadanie'
            ]
        );

        $data = array_merge(
            $request->all(),
            ['wedding_id' => auth()->user()->wedding_id]
        );



        Task::create($data);
        return redirect()->route('task.index')
            ->with('success','Zadanie zostało dodane.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

        $task = Task::find($id);

        return view('tasks.show', compact('task'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Task::find($id);

        return view('tasks.edit', compact('task'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        //dd($task);

       if($request->has('title')){
           request()->validate([
               'title' => ['required','unique:tasks,title,'.$task->id.',id,wedding_id,'.auth()->user()->wedding_id],
               'due_date_planned' => 'required'

           ],
               [
                   'title.required' => 'Nazwa zadania jest wymagana.',
                   'due_date_planned.required' => 'Wybór daty jest wymagany',
                   'title.unique' => 'Istnieje już takie zadanie'
               ]
           );

       }

        $task->update($request->all());

        return redirect()->route('task.index');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        Task::find($id)->delete();

        return redirect()->route('task.index')
            ->with('success','Zadanie zostało usunięte');
    }

}
