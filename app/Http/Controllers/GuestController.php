<?php

namespace App\Http\Controllers;

use App\Guest;
use App\Wedding;
use Illuminate\Http\Request;

class GuestController extends Controller
{

    public function _construct() {

        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $guests = Guest::where('wedding_id','=', auth()->user()->wedding_id);
        $all_guests = Guest::where('wedding_id','=', auth()->user()->wedding_id)->count();
        $filtered_guests = $all_guests;
        $queries = [];

        $columns = [
          'confirmation',
          'special_diet',
          'accommodation',
          'transport',
          'party'
        ];

        foreach ($columns as $column){

            if(request()->has($column)) {
                $guests = $guests->where($column, request($column));
                $filtered_guests = $guests->where($column, request($column))->count();
                $queries[$column] = request($column);
            }
        }

        $guests = $guests->paginate(10)->appends($queries);

        return view('guests.index',compact(['guests', 'all_guests', 'filtered_guests']))
            ->with('i', ($request->input('page', 1) - 1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('guests.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validation();

        $confirmation = isset($request->confirmation) ? 1 : 0;
        $accommodation = isset($request->accommodation) ? 1 : 0;
        $special_diet = isset($request->special_diet) ? 1 : 0;
        $transport = isset($request->transport) ? 1 : 0;


        $data = array_merge(
            $request->all(),
            ['wedding_id' => auth()->user()->wedding_id],
            ['confirmation' => $confirmation],
            ['accommodation' => $accommodation],
            ['special_diet' => $special_diet],
            ['transport' => $transport]
        );

        Guest::create($data);
        return redirect()->route('guest.index')
            ->with('success','Gość został dodany.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Guest  $guest
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

        $guest = Guest::find($id);
        $age = $guest->age == 1 ? 'Dorosły' : 'Dziecko';

        if($guest->party == 'her')
            $party = 'Panna Młoda';
        elseif($guest->party == 'his')
            $party = 'Pan Młody';
        else
            $party = 'Wspólny';

        $confirmationCheck = $guest->confirmation == 1 ? 'checked' : '';
        $special_dietCheck = $guest->special_diet == 1 ? 'checked' : '';
        $accommodationCheck = $guest->accommodation == 1 ? 'checked' : '';
        $transportCheck = $guest->transport == 1 ? 'checked' : '';

        return view('guests.show', compact([
            'guest',
            'age',
            'party',
            'confirmationCheck',
            'special_dietCheck',
            'accommodationCheck',
            'transportCheck',
        ]));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Guest  $guest
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $guest = Guest::find($id);

        $confirmationCheck = $guest->confirmation == 1 ? 'checked' : '';
        $special_dietCheck = $guest->special_diet == 1 ? 'checked' : '';
        $accommodationCheck = $guest->accommodation == 1 ? 'checked' : '';
        $transportCheck = $guest->transport == 1 ? 'checked' : '';

        return view('guests.edit', compact([
            'guest',
            'confirmationCheck',
            'special_dietCheck',
            'accommodationCheck',
            'transportCheck',
        ]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Guest  $guest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $this->validation();


        $confirmation = isset($request->confirmation) ? 1 : 0;
        $accommodation = isset($request->accommodation) ? 1 : 0;
        $special_diet = isset($request->special_diet) ? 1 : 0;
        $transport = isset($request->transport) ? 1 : 0;


        $data = array_merge(
            $request->all(),
            ['wedding_id' => auth()->user()->wedding_id],
            ['confirmation' => $confirmation],
            ['accommodation' => $accommodation],
            ['special_diet' => $special_diet],
            ['transport' => $transport]
        );

        $guest = Guest::find($id);
        $guest->update($data);

        return redirect()->route('guest.index')
            ->with('success','Gość został edytowany.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Guest  $guest
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Guest::find($id)->delete();

        return redirect()->route('guest.index')
            ->with('success','Gość został usunięty');
    }

    public function validation()
    {
        request()->validate([
            'name' => 'required',
            'surname' => 'required',
            'age' => '',
            'email' => 'nullable|email',
            'phone_number' => '',
            'street_address' => '',
            'postal_code' => '',
            'city' => '',
            'party' => '',
            'confirmation' => '',
            'accommodation' => '',
            'special_diet' => '',
            'transport' => '',
            'note' => ''
        ],
            [
                'name.required' => 'Imię jest wymagane.',
                'surname.required' => 'Nazwisko jest wymagane.',
                'email' => 'Niepoprawny format email.'
            ]
        );
    }
}
