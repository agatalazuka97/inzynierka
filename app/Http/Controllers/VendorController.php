<?php

namespace App\Http\Controllers;

use App\Vendor;
use App\VendorCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $vendors = DB::table('vendors')
            -> join('vendor_categories', 'vendor_category_id', '=', 'vendor_categories.id')
            -> select('vendors.id as id', 'vendors.name as name', 'vendors.email', 'vendors.phone_number',
                'vendors.street_address', 'vendors.postal_code', 'vendors.city',
                'vendor_categories.name as category', 'vendor_categories.icon')
            -> where('vendors.wedding_id', '=', auth()->user()->wedding_id)
            -> orderBy('category')
            -> get();

        $vendor_info = DB::table('vendors')
            -> join('vendor_categories', 'vendor_category_id', '=', 'vendor_categories.id')
            -> select('vendor_categories.name as category', DB::raw('count(*) as categories_count'))
            -> where('vendors.wedding_id', '=', auth()->user()->wedding_id)
            -> groupBy('vendor_categories.name')
            -> orderBy('category')
            -> get();


        if(request()->has('category')){
            $vendors = $vendors->where('category', request('category'));
        }

        return view('vendors.index', compact(['vendors', 'vendor_info']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $vendor_categories = DB::table('vendor_categories')
            ->whereNull('wedding_id')
            ->orWhere('wedding_id', auth()->user()->wedding_id)
            ->get();

        return view('vendors.create', compact(['vendor_categories']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $this->validation();

        $data = array_merge(
          $request->all(),
          ['wedding_id' => auth()->user()->wedding_id]
        );



        Vendor::create($data);
        return redirect()->route('vendor.index')
            ->with('success','Usługodawca został dodany.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function show(Vendor $vendor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $vendor = Vendor::find($id);

        $selected_category = $vendor->vendor_category_id;

        $vendor_categories = DB::table('vendor_categories')
            ->whereNull('wedding_id')
            ->orWhere('wedding_id', auth()->user()->wedding_id)
            ->get();

        return view('vendors.edit', compact(['vendor', 'selected_category','vendor_categories']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validation();

        $data = array_merge(
            $request->all(),
            ['wedding_id' => auth()->user()->wedding_id]
        );



        $vendor = Vendor::find($id);
        $vendor -> update($data);
        return redirect()->route('vendor.index')
            ->with('success','Usługodawca został edytowany.');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Vendor::find($id)->delete();

        return redirect()->route('vendor.index')
            ->with('success','Usługodawca został usunięty');
    }

    public function validation()
    {
        request()->validate([
            'name' => ['required','unique:vendors,name,NULL,id,wedding_id,'.auth()->user()->wedding_id],
            'email' => 'nullable|email',
            'vendor_category_id' => 'required',
            'phone_number' => '',
            'street_address' => '',
            'postal_code' => '',
            'city' => '',
        ],
            [
                'name.required' => 'Nazwa firmy jest wymagana.',
                'name.unique' => 'Istnieje firma o takiej nazwie',
                'vendor_category_id.required' => 'Wybór kategorii jest wymagany'
            ]
        );
    }
}
