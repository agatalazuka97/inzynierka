<?php

namespace App\Http\Controllers;

use App\CostCategory;
use Illuminate\Http\Request;

class CostCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CostCategory  $costCategory
     * @return \Illuminate\Http\Response
     */
    public function show(CostCategory $costCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CostCategory  $costCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(CostCategory $costCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CostCategory  $costCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CostCategory $costCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CostCategory  $costCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(CostCategory $costCategory)
    {
        //
    }
}
