<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Local extends Model
{
    //
    protected $guarded = [];

    public function weddings()
    {

        return $this->hasMany(Wedding::class);
    }

}
