<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    //
    protected $guarded = [];
    protected $fillable = [
        'title', 'note', 'due_date_planned', 'wedding_id', 'status'
    ];

    public function wedding(){

        return $this->belongsTo(Wedding::class);
    }
}
