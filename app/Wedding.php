<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wedding extends Model
{
    //
    protected $guarded = [];

    protected $fillable = [
        'local_id', 'wedding_date', 'budget', 'partner_name',
    ];

    public function local()
    {
        return $this->belongsTo(Local::class);
    }

    public function cost_categories()
    {
        return $this->hasMany(CostCategory::class);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    public function vendor_categories()
    {
        return $this->hasMany(VendorCategory::class);
    }

    public function vendors()
    {
        return $this->hasMany(Vendor::class);
    }

    public function tables()
    {
        return $this->hasMany(Table::class);
    }

    public function guests()
    {
        return $this->hasMany(Guest::class);
    }
}
