<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorCategory extends Model
{
    protected $guarded = [];
    protected $fillable = [
        'name', 'wedding_id'
    ];

    public function wedding()
    {
        return $this->belongsTo(Wedding::class);
    }

    public function vendors()
    {
        return $this->hasMany(Vendor::class);
    }


}
