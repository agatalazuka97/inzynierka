<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cost extends Model
{
    //
    protected $guarded = [];

    public function wedding()
    {
        return $this->belongsTo(Wedding::class);
    }

    public function cost_category(){

        return $this->belongsTo(CostCategory::class);
    }
}
