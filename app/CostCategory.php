<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CostCategory extends Model
{
    //
    protected $guarded = [];

    public function wedding()
    {
        return $this->belongsTo(Wedding::class);
    }

    public function costs()
    {
        return $this->hasMany(Cost::class);

    }
}
