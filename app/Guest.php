<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guest extends Model
{
    protected $guarded = [];

    protected $fillable = [
        'name', 'surname', 'age', 'email', 'phone_number', 'street_address', 'city', 'postal_code',
        'confirmation', 'accommodation', 'transport', 'special_diet', 'party', 'note', 'wedding_id'
    ];

    public function wedding()
    {
        return $this->belongsTo(Wedding::class);
    }

    public function table()
    {
        return $this->belongsTo(Table::class);
    }
}
