<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Table extends Model
{
    //
    protected $guarded = [];
    public function wedding()
    {
        return $this->belongsTo(Wedding::class);
    }

    public function guests()
    {
        return $this->hasMany(Guest::class);
    }
}
