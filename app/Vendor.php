<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    //
    protected $guarded = [];
    protected $fillable = [
      'name', 'vendor_category_id', 'email', 'phone_number', 'street_address', 'city', 'postal_code', 'wedding_id'
    ];
    public function wedding()
    {
        return $this->belongsTo(Wedding::class);
    }

    public function vendor_category()
    {
        return $this->belongsTo(VendorCategory::class);
    }
}
