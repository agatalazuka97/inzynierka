<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\CostCategory;
use Faker\Generator as Faker;

$factory->define(CostCategory::class, function (Faker $faker) {
    return [
        //

        'wedding_id' => function(){
            return factory(\App\Wedding::class)->create()->id;
        },
        'name' => $faker->word,
    ];
});
