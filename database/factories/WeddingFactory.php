<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Local;
use App\Wedding;
use Faker\Generator as Faker;

$factory->define(Wedding::class, function (Faker $faker) {
    return [
        //
        'local_id' => function(){
        return factory(Local::class)->create()->id;
        },
        'wedding_date' => $faker->dateTimeBetween('now', '+4 years'),
        'budget' => $faker->numberBetween($min = 5000, $max = 30000),
        'partner_name' => $faker->firstNameMale,

    ];
});
