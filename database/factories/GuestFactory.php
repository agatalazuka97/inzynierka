<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Guest;
use Faker\Generator as Faker;

$factory->define(Guest::class, function (Faker $faker) {
    return [
        'wedding_id' => function(){
            return factory(\App\Wedding::class)->create()->id;
        },
        'table_id' => function(){
            return factory(\App\Table::class)->create()->id;
        },
        'name' => $faker->firstName,
        'surname' => $faker->lastName,
        'city' => $faker->city,
        'street_address' => $faker->streetAddress,
        'postal_code' => $faker->postcode,
        'email' => $faker->email,
        'phone_number' => $faker->phoneNumber,
        'confirmation' => $faker->boolean,
        'age' => $faker->boolean,
        'special_diet' => $faker->boolean,
        'accommodation' => $faker->boolean,
        'transport' => $faker->boolean,
        'note' => $faker->text($maxNbChars = 50),
        'party' => $faker->randomElement(['his' ,'her', 'both']),

        ];
});
