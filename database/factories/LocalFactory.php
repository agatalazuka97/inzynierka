<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Local;
use Faker\Generator as Faker;

$factory->define(Local::class, function (Faker $faker) {
    return [
        //
        'name' => $faker->company,
        'city' => $faker->city,
        'street_address' => $faker->streetAddress,
        'postal_code' => $faker->postcode,
    ];
});
