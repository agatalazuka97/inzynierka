<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Task;
use Faker\Generator as Faker;

$factory->define(Task::class, function (Faker $faker) {
    return [
        //
        'wedding_id' => function(){
            return factory(\App\Wedding::class)->create()->id;
        },
        'title' => $faker->sentence,
        'note' => $faker->text($maxNbChars = 150),
        'status' => $faker->boolean,
        'due_date_planned' => $faker->dateTimeBetween('now', '+4 years'),
        'due_date_actual' => $faker->dateTimeBetween('now', '+4 years'),
    ];
});
