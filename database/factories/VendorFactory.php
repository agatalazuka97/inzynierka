<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Vendor;
use Faker\Generator as Faker;

$factory->define(Vendor::class, function (Faker $faker) {
    return [
        //
        'wedding_id' => function(){
            return factory(\App\Wedding::class)->create()->id;
        },
        'name' => $faker->company,
        'email' => $faker->companyEmail,
        'phone_number' => $faker->phoneNumber,
        'city' => $faker->city,
        'street_address' => $faker->streetAddress,
        'postal_code' => $faker->postcode,
        'vendor_category_id' => function(){
            return factory(\App\VendorCategory::class)->create()->id;
        },
    ];
});
