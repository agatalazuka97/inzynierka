<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\VendorCategory;
use Faker\Generator as Faker;

$factory->define(VendorCategory::class, function (Faker $faker) {
    return [
        //
        'wedding_id' => function(){
            return factory(\App\Wedding::class)->create()->id;
        },
        'name' => $faker->word,

    ];
});
