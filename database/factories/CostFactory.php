<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Cost;
use Faker\Generator as Faker;

$factory->define(Cost::class, function (Faker $faker) {
    return [
        //
        'cost_category_id' => function(){
            return factory(\App\CostCategory::class)->create()->id;
        },
        'wedding_id' => function(){
            return factory(\App\Wedding::class)->create()->id;
        },
        'title' => $faker->sentence,
        'cost_planned' => $faker->numberBetween($min = 50, $max = 2000),
        'cost_actual' => $faker->numberBetween($min = 50, $max = 2000),
        'status' => $faker->boolean,
        'note' => $faker->text($maxNbChars = 150),
    ];
});
