<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Table;
use Faker\Generator as Faker;

$factory->define(Table::class, function (Faker $faker) {
    return [
        //
        'wedding_id' => function(){
            return factory(\App\Wedding::class)->create()->id;
        },
        'name' => $faker->word,
        'seats_number' => $faker->numberBetween($min=2, $max=20)
    ];
});
