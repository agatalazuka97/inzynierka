<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call([
             LocalsSeeder::class,
             WeddingsSeeder::class,
             TasksSeeder::class,
             CostCategoriesSeeder::class,
             CostsSeeder::class,
             VendorCategoriesSeeder::class,
             VendorsSeeder::class,
             TablesSeeder::class,
             GuestSeeder::class,
             UsersSeeder::class,
         ]);
    }
}
