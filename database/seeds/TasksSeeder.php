<?php

use Illuminate\Database\Seeder;

class TasksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $weddings = \App\Wedding::all();

        foreach ($weddings as $wedding) {
            factory(\App\Task::class,5)->create([
                'wedding_id' => $wedding->id
            ]);
        }
    }
}
