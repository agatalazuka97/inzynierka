<?php

use Illuminate\Database\Seeder;

class GuestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $weddings = \App\Wedding::all();

        foreach ($weddings as $wedding) {
            $weddingTablesID = $wedding->tables()->pluck('id');
            foreach ($weddingTablesID as $weddingTableID) {
                factory(\App\Guest::class,mt_rand(2,5))->create([
                    'wedding_id' => $wedding->id,
                    'table_id' => $weddingTableID
                ]);
            }
            factory(\App\Guest::class,mt_rand(2,5))->create([
                'wedding_id' => $wedding->id,
                'table_id' => null,
            ]);
        }
    }
}
