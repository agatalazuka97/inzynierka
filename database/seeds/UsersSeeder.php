<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $weddings = \App\Wedding::all();

        foreach ($weddings as $wedding) {
            factory(\App\User::class,2)->create([
                'wedding_id' => $wedding->id
            ]);
        }

    }

}
