<?php

use Illuminate\Database\Seeder;

class WeddingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $locals = \App\Local::all();

        foreach ($locals as $local){
            factory(\App\Wedding::class,2)->create([
                'local_id' => $local->id
            ]);
        }
    }
}
