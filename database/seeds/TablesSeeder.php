<?php

use Illuminate\Database\Seeder;

class TablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $weddings = \App\Wedding::all();

        foreach ($weddings as $wedding) {
            factory(\App\Table::class,4)->create([
                'wedding_id' => $wedding->id
            ]);
        }
    }
}
