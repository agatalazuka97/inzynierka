<?php

use Illuminate\Database\Seeder;

class VendorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $weddings = \App\Wedding::all();

        $globalCategoriesID = \Illuminate\Support\Facades\DB::table('vendor_categories')
            ->where('wedding_id', '=', null)
            ->get()
            ->pluck('id');

        foreach ($weddings as $wedding){
            foreach ($globalCategoriesID as $globalCategoryID) {
                factory(\App\Vendor::class,1)->create([
                    'vendor_category_id' => $globalCategoryID,
                    'wedding_id' => $wedding->id
                ]);
            }
            $weddingCategory = $wedding->vendor_categories()->first();
            if(!is_null($weddingCategory)) {
                factory(\App\Vendor::class, 1)->create([
                    'vendor_category_id' => $weddingCategory->id,
                    'wedding_id' => $wedding->id
                ]);
            }

        }
    }
    //
}
