<?php

use Illuminate\Database\Seeder;

class VendorCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\VendorCategory::class,10)->create([
            'wedding_id' => null
        ]);

        $weddings = \App\Wedding::all();

        for($i = 0; $i<sizeof($weddings); $i+=3) {
            factory(\App\VendorCategory::class)->create([
                'wedding_id' => $weddings[$i]->id
            ]);
        }
    }
}
