<?php

use Illuminate\Database\Seeder;

class LocalsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Local::class, 10)->create();
    }
}
