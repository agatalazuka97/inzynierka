<?php

use Illuminate\Database\Seeder;

class CostsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $weddings = \App\Wedding::all();

        $globalCategoriesID = \Illuminate\Support\Facades\DB::table('cost_categories')
            ->where('wedding_id', '=', null)
            ->get()
            ->pluck('id');

        foreach ($weddings as $wedding){
            foreach ($globalCategoriesID as $globalCategoryID) {
                factory(\App\Cost::class,5)->create([
                    'cost_category_id' => $globalCategoryID,
                    'wedding_id' => $wedding->id
                ]);
            }
            $weddingCategory = $wedding->cost_categories()->first();
            if(!is_null($weddingCategory)) {
                factory(\App\Cost::class, 5)->create([
                    'cost_category_id' => $weddingCategory->id,
                    'wedding_id' => $wedding->id
                ]);
            }

        }
    }
}
