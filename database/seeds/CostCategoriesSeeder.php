<?php

use Illuminate\Database\Seeder;

class CostCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\CostCategory::class,5)->create([
           'wedding_id' => null
        ]);

        $weddings = \App\Wedding::all();

        for($i = 0; $i<sizeof($weddings); $i+=3) {
            factory(\App\CostCategory::class)->create([
                'wedding_id' => $weddings[$i]->id
            ]);
        }

    }
}
