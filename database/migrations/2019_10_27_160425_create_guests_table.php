<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGuestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('wedding_id');
            $table->unsignedBigInteger('table_id')->nullable();
            $table->string('name');
            $table->string('surname');
            $table->boolean('age')->nullable();
            $table->string('city')->nullable();
            $table->string('street_address')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('email')->nullable();
            $table->string('phone_number')->nullable();
            $table->boolean('confirmation')->default(0);
            $table->boolean('special_diet')->default(0);
            $table->boolean('accommodation')->default(0);
            $table->boolean('transport')->default(0);
            $table->enum('party', array('his', 'her', 'both'))->nullable();
            $table->text('note')->nullable();

            $table->foreign('wedding_id')->references('id')->on('weddings');
            $table->foreign('table_id')->references('id')->on('tables');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guests');
    }
}
