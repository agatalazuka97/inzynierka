<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('wedding_id')->nullable();
            $table->string('name');
            $table->string('icon')->default('fas fa-star');
            $table->timestamps();

            $table->foreign('wedding_id')->references('id')->on('weddings');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_categories');
    }
}
