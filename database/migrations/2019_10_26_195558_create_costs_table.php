<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('costs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('cost_category_id');
            $table->unsignedBigInteger('wedding_id');
            $table->string('title');
            $table->decimal('cost_planned');
            $table->decimal('cost_actual')->nullable();
            $table->boolean('status')->default(0);
            $table->text('note')->nullable();
            $table->timestamps();

            $table->foreign('wedding_id')->references('id')->on('weddings');
            $table->foreign('cost_category_id')->references('id')->on('cost_categories');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('costs');
    }
}
