@extends('layouts.app')


@section('content')
    <div class="container">

        <div class="row justify-content-center">

            <div class="col-md-8">

                <div class="pb-3">

                    <a class="btn btn-outline-secondary" href="{{ route('task.index') }}"><i class="fas fa-arrow-left"></i> Powrót</a>

                </div>

                @if (count($errors) > 0)

                    <div class="alert alert-danger">

                        <strong>Ups!</strong> Wprowadzone dane są niepoprawne.<br><br>

                        <ul>

                            @foreach ($errors->all() as $error)

                                <li>{{ $error }}</li>

                            @endforeach

                        </ul>

                    </div>

                @endif


                <div class="card">

                    <div class="card-header text-center bg-white">

                        <h3> Edytuj zadanie </h3>

                    </div>

                    <div class="card-body">

                        {!! Form::model($task, ['method' => 'PATCH','route' => ['task.update', $task->id]]) !!}


                        <div class="form-row">

                            <div class="form-group col-12">

                                <strong>Nazwa zadania:</strong>

                                {!! Form::text('title', null, array('placeholder' => 'Nazwa zadania','class' => 'form-control')) !!}

                            </div>

                        </div>

                        <div class="form-row">

                            <div class="form-group col-12">

                                <strong>Data wykonania:</strong>

                                {!! Form::date('due_date_planned', null, array('placeholder' => 'Nazwa zadania','class' => 'form-control')) !!}

                            </div>

                        </div>

                        <div class="form-row pt-2">

                            <div class="form-group col-12">

                                <strong>Notatka:</strong>

                                <textarea class="form-control" id="note" name="note" rows="2"> {{ $task->note }} </textarea>

                            </div>

                        </div>


                        <div class=" text-center pt-2">

                            <button type="submit" class="btn btn-primary">Zapisz</button>

                        </div>
                        {!! Form::close() !!}</div>
                </div>
            </div>
        </div>
    </div></div>


@endsection