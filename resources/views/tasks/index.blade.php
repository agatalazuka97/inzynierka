@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="row justify-content-center pb-3">

            <a class="btn btn-primary btn-lg cl-white" href="{{ route('task.create') }}"><i class="fas fa-plus"
                                                                                            style="color: white"></i>
                Dodaj zadanie</a>

        </div>


        <div class="col-md-12"><h4 class="pt-4">Zrealizowane</h4>

            <ui class="list-group">
                @foreach ($overdue_tasks as $overdue_task)

                    <li class="list-group-item d-flex flex-column">
                        <div class="row d-flex justify-content-between">
                            <div>
                                <div class="custom-control custom-checkbox form-check-inline">

                                    <input type="checkbox" class="custom-control-input" id="{{ $overdue_task->id }}"
                                           name="status" value="0" checked>
                                    <label class="custom-control-label"
                                           for="{{$overdue_task->id}}"><del>{{ $overdue_task->title }}</del></label>

                                </div>
                            </div>

                            {!! Form::open(['method' => 'DELETE','route' => ['task.destroy', $overdue_task->id],'style'=>'display:inline']) !!}
                            {!! Form::button('<i class="fas fa-trash "></i>', ['type' => 'submit', 'class' => 'btn btn-link btn-sm']) !!}
                            {!! Form::close() !!}

                        </div>
                        <div class="text-muted">{{ $overdue_task->due_date_planned }}
                            <a class="btn btn-sm btn-link" href="{{ route('task.show',$overdue_task->id) }}">Pokaż</a>
                            <a class="btn btn-sm btn-link" href="{{ route('task.edit',$overdue_task->id) }}">Edytuj</a>
                        </div>

                    </li>
                @endforeach
            </ui>
        </div>


        @foreach ($tasks as $time => $tasks_list)
            <div class="col-md-12"><h4 class="pt-4">{{ $time }} </h4>

                <ui class="list-group">
                    @foreach ($tasks_list as $task)

                        <li class="list-group-item d-flex flex-column">
                            <div class="row d-flex justify-content-between">
                                <div>
                                    <div class="custom-control custom-checkbox form-check-inline">

                                        <input type="checkbox" class="custom-control-input" id="{{ $task->id }}"
                                               name="status" value="1" >
                                        <label class="custom-control-label"
                                               for="{{$task->id}}">{{ $task->title }}</label>

                                    </div>
                                </div>

                                {!! Form::open(['method' => 'DELETE','route' => ['task.destroy', $task->id],'style'=>'display:inline']) !!}
                                {!! Form::button('<i class="fas fa-trash "></i>', ['type' => 'submit', 'class' => 'btn btn-link btn-sm']) !!}
                                {!! Form::close() !!}

                            </div>
                            <div class="text-muted">{{ $task->due_date_planned }}
                                <a class="btn btn-sm btn-link" href="{{ route('task.show',$task->id) }}">Pokaż</a>
                                <a class="btn btn-sm btn-link" href="{{ route('task.edit',$task->id) }}">Edytuj</a>
                            </div>
                        </li>
                    @endforeach
                </ui>
            </div>
        @endforeach

    </div>

    <script src="http://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous">
    </script>


    <script>
        jQuery(document).ready(function(){

            jQuery("input[type='checkbox']").change(function(){

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                });
                let patch = "{{ url('/task') }}" + '/' + $(this).attr('id');
                $.ajax({
                    url: patch  ,
                    method: 'patch',
                    data: {
                        'status' : $(this).is(':checked') ? 1 : 0

                    },
                    success: function(result){
                        console.log(result);

                    },
                    complete: function (result) {
                        location.reload();
                    }
                });

            });


        });


    </script>

@endsection

