@extends('layouts.app')

@section('content')

    <div class="container">



        <div class="row justify-content-between d-flex align-items-baseline pb-1">

            <div class="row">

                <div class="dropdown pl-1">
                    <button class="btn btn-outline-secondary btn-sm dropdown-toggle {{ (request()->has('confirmation')) ? 'active' : '' }}"
                            type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Przybycie
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="/guest/?confirmation=1">Potwierdzone</a>
                        <a class="dropdown-item" href="/guest/?confirmation=0">Nie potwierdzone</a>
                    </div>
                </div>

                <div class="dropdown pl-1">
                    <button class="btn btn-outline-secondary btn-sm dropdown-toggle {{ (request()->has('special_diet')) ? 'active' : '' }} "
                            type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Dieta
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="/guest/?special_diet=1">Dieta specjalna</a>
                        <a class="dropdown-item" href="/guest/?special_diet=0">Brak diety</a>
                    </div>
                </div>

                <div class="dropdown pl-1">
                    <button class="btn btn-outline-secondary btn-sm dropdown-toggle {{ (request()->has('accommodation')) ? 'active' : '' }} "
                            type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Nocleg
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="/guest/?accommodation=1">Wymagany</a>
                        <a class="dropdown-item" href="/guest/?accommodation=0">Nie wymagany</a>
                    </div>
                </div>

                <div class="dropdown pl-1">
                    <button class="btn btn-outline-secondary btn-sm dropdown-toggle {{ (request()->has('transport')) ? 'active' : '' }} "
                            type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Transport
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="/guest/?transport=1">Wymagany</a>
                        <a class="dropdown-item" href="/guest/?transport=0">Nie wymagany</a>
                    </div>
                </div>

                <div class="dropdown pl-1">
                    <button class="btn btn-outline-secondary btn-sm dropdown-toggle {{ (request()->has('party')) ? 'active' : '' }} "
                            type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Zapraszający
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="/guest/?party=both">Wspólny</a>
                        <a class="dropdown-item" href="/guest/?party=her">Panna Młoda</a>
                        <a class="dropdown-item" href="/guest/?party=his">Pan Młody</a>
                    </div>
                </div>

                <div class="pl-1">
                    <a class="btn btn-outline-primary btn-sm " href="/guest">Wyczyść filtry</a>
                </div>

            </div>

            <a class="btn btn-primary btn-lg" href="{{ route('guest.create') }}" style="color:white">
                <i class="fas fa-plus" style="color: white"></i> Dodaj gościa</a>

        </div>



        @if ($message = Session::get('success'))

            <div class="alert alert-success">

                <p>{{ $message }}</p>

            </div>

        @endif

        <div class="row justify-content-center pt-1">
            <div class="col-md-12 table-responsive">
                <table class="table table-hover">

                    <thead>
                        <tr>

                            <th>Lp.</th>

                            <th>Imię i nazwisko</th>

                            <th class="text-center">Przybycie</th>

                            <th class="text-center">Dieta</th>

                            <th class="text-center">Nocleg</th>

                            <th class="text-center">Transport</th>

                            <th class="text-center">Zapraszający</th>

                            <th class="text-center">Opcje</th>

                        </tr>
                    </thead>

                    <tbody>
                        @foreach ($guests as $key => $guest)

                            <tr>

                                <td>{{ ++$i }}</td>

                                <td>{{ $guest->name }} {{ $guest->surname }} </td>

                                <td class="party text-center">

                                    <i class="fas fa-check-circle fa-lg" {{ intval($guest->confirmation) == 1 ? 'style=color:yellowgreen' : '' }}></i>

                                </td>

                                <td class="party text-center">

                                        <i class="fas fa-utensils fa-lg" {{ intval($guest->special_diet) == 1 ? 'style=color:yellowgreen' : '' }}> </i>

                                </td>

                                <td class="party text-center">

                                        <i class="fas fa-bed fa-lg" {{ intval($guest->accommodation) == 1 ? 'style=color:yellowgreen' : '' }}> </i>

                                </td>

                                <td class="party text-center">

                                        <i class="fas fa-car fa-lg" {{ intval($guest->transport) == 1 ? 'style=color:yellowgreen' : '' }}> </i>

                                </td>

                                <td class="party text-center">
                                    @if($guest->party == 'her')
                                        <i class="fas fa-female fa-lg" style="color:pink"> </i>
                                    @elseif($guest->party == 'his')
                                        <i class="fas fa-male fa-lg" style="color:cornflowerblue"> </i>
                                    @else
                                        <i class="fas fa-female fa-lg" style="color:pink"></i><i class="fas fa-male fa-lg" style="color:cornflowerblue"> </i>
                                    @endif
                                </td>

                                <td class="text-center">

                                    <a class="btn btn-sm btn-info" href="{{ route('guest.show',$guest->id) }}"><i class="fas fa-search" style="color: white"></i></a>

                                    <a class="btn btn-sm btn-primary" href="{{ route('guest.edit',$guest->id) }}"><i class="fas fa-edit" style="color: white"></i></a>

                                    {!! Form::open(['method' => 'DELETE','route' => ['guest.destroy', $guest->id],'style'=>'display:inline']) !!}

                                    {!! Form::button('<i class="fas fa-trash" style="color: white"></i>', ['type' => 'submit', 'class' => 'btn btn-sm btn-danger']) !!}

                                    {!! Form::close() !!}

                                </td>

                            </tr>

                        @endforeach
                    </tbody>

                </table>

                <div class="row">
                    <p class="text-muted">Liczba gości: {{ $filtered_guests }} z {{ $all_guests }}</p>
                </div>

            </div>
        </div>


        <div class="row justify-content-center">

                {{ $guests->links() }}

        </div>

    </div>



@endsection