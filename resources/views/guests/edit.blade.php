@extends('layouts.app')


@section('content')
    <div class="container">

        <div class="row justify-content-center">

            <div class="col-md-8">

                <div class="pb-3">

                    <a class="btn btn-outline-secondary" href="{{ route('guest.index') }}"><i class="fas fa-arrow-left"></i> Powrót</a>

                </div>

                @if (count($errors) > 0)

                    <div class="alert alert-danger">

                        <strong>Whoops!</strong> There were some problems with your input.<br><br>

                        <ul>

                            @foreach ($errors->all() as $error)

                                <li>{{ $error }}</li>

                            @endforeach

                        </ul>

                    </div>

                @endif


                <div class="card">

                    <div class="card-header text-center bg-white">

                        <h3> Edytuj gościa </h3>

                    </div>

                    <div class="card-body">

                        {!! Form::model($guest, ['method' => 'PATCH','route' => ['guest.update', $guest->id]]) !!}

                        <h4>Dane personalne</h4>

                        <div class="form-row d-flex align-items-baseline">

                            <div class="form-group col-md-5">

                                <strong>Imię:</strong>

                                {!! Form::text('name', null, array('placeholder' => 'Imię','class' => 'form-control')) !!}

                            </div>

                            <div class="form-group col-md-5">

                                <strong>Nazwisko:</strong>

                                {!! Form::text('surname', null, array('placeholder' => 'Nazwisko','class' => 'form-control')) !!}

                            </div>

                            <div class="form-group col-md-2">

                                <strong>Wiek:</strong>

                                <div>{!! Form::select('age', array('1' => 'Dorosły', '0' => 'Dziecko'), null, array('class' => 'custom-select')) !!} </div>
                            </div>

                        </div>

                        <h4 class="pt-3">Dane kontaktowe</h4>

                        <div class="form-row">

                            <div class="form-group col-md-6">

                                <strong>Email:</strong>

                                {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}

                            </div>

                            <div class="form-group col-md-6">

                                <strong>Telefon:</strong>

                                {!! Form::text('phone_number', null, array('placeholder' => 'Telefon','class' => 'form-control')) !!}

                            </div>

                        </div>

                        <div class = "form-row">

                            <div class ="form-group col-md-5">

                                <strong>Adres:</strong>

                                {!! Form::text('street_address', null, array('placeholder' => 'Adres','class' => 'form-control')) !!}

                            </div>

                            <div class ="form-group col-md-3">

                                <strong>Kod pocztowy:</strong>

                                {!! Form::text('postal_code', null, array('placeholder' => 'Kod pocztowy','class' => 'form-control')) !!}

                            </div>

                            <div class ="form-group col-md-4">

                                <strong>Miejscowość:</strong>

                                {!! Form::text('city', null, array('placeholder' => 'Miejscowość','class' => 'form-control')) !!}

                            </div>


                        </div>

                        <h4 class="pt-3">Pozostałe informacje</h4>

                        <div class="form-row">

                            <div class="form-group col-md-3">

                                <strong>Zapraszający:</strong>

                                <div>{!! Form::select('party', array('both' => 'Wspólny', 'her' => 'Panna Młoda', 'his' => 'Pan Młody'), null, array('class' => 'custom-select')) !!} </div>

                            </div>


                        </div>


                        <div class="form-row">

                            <div class="custom-control custom-checkbox form-check-inline">

                                <input type="checkbox" class="custom-control-input" id="confirmation" name="confirmation" value="1" {{ $confirmationCheck }}>
                                <label class="custom-control-label" for="confirmation">Przybycie</label>

                            </div>


                            <div class="custom-control custom-checkbox form-check-inline">

                                <input type="checkbox" class="custom-control-input" id="special_diet" name="special_diet" value="1" {{ $special_dietCheck }}>
                                <label class="custom-control-label" for="special_diet">Dieta</label>

                            </div>

                            <div class="custom-control custom-checkbox form-check-inline">

                                <input type="checkbox" class="custom-control-input" id="accommodation" name="accommodation" value="1" {{ $accommodationCheck }}>
                                <label class="custom-control-label" for="accommodation">Nocleg</label>

                            </div>

                            <div class="custom-control custom-checkbox form-check-inline">

                                <input type="checkbox" class="custom-control-input" id="transport" name="transport" value="1" {{ $transportCheck }}>
                                <label class="custom-control-label" for="transport">Transport</label>

                            </div>

                        </div>

                        <div class="form-row pt-2">

                            <div class="form-group col-md-12">

                                <strong>Notatka:</strong>

                                <textarea class="form-control" id="note" name="note" rows="2">{{ $guest->note }}</textarea>

                            </div>

                        </div>

                        <div class="col-md-12 text-center">

                            <button type="submit" class="btn btn-primary">Edytuj</button>

                        </div>
                        {!! Form::close() !!}</div>
                </div>
            </div>
        </div>
    </div></div>


@endsection