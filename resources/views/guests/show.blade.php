@extends('layouts.app')


@section('content')
    <div class="container">

        <div class="row justify-content-center">

            <div class="col-md-8">

                <div class="pb-3">

                    <a class="btn btn-outline-secondary" href="{{ route('guest.index') }}"><i class="fas fa-arrow-left"></i> Powrót</a>

                </div>

                <div class="card">

                    <div class="card-header text-center bg-white">

                        <h3> Twój gość </h3>

                    </div>

                    <div class="card-body">

                        <h4>Dane personalne</h4>

                        <div class="form-row d-flex align-items-baseline">

                            <div class="form-group col-md-5">

                                <strong>Imię:</strong>

                                {{ $guest->name }}

                            </div>

                            <div class="form-group col-md-5">

                                <strong>Nazwisko:</strong>

                                {{ $guest -> surname }}

                            </div>

                            <div class="form-group col-md-2">

                                <strong>Wiek:</strong>

                                {{ $age }}

                            </div>

                        </div>

                        <h4 class="pt-3">Dane kontaktowe</h4>

                        <div class="form-row">

                            <div class="form-group col-md-6">

                                <strong>Email:</strong>

                                {{ $guest -> email }}

                            </div>

                            <div class="form-group col-md-6">

                                <strong>Telefon:</strong>

                                {{ $guest -> phone_number }}

                            </div>

                        </div>

                        <div class = "form-row">

                            <div class ="form-group col-md-5">

                                <strong>Adres:</strong>

                                {{ $guest -> street_address }}

                            </div>

                            <div class ="form-group col-md-3">

                                <strong>Kod pocztowy:</strong>

                                {{ $guest -> postal_code }}

                            </div>

                            <div class ="form-group col-md-4">

                                <strong>Miejscowość:</strong>

                                {{ $guest -> city }}

                            </div>


                        </div>

                        <h4 class="pt-3">Pozostałe informacje</h4>

                        <div class="form-row">

                            <div class="form-group col-md-6">

                                <strong>Zapraszający:</strong>

                                {{ $party }}

                            </div>

                        </div>


                        <div class="form-row">

                            <div class="custom-control custom-checkbox form-check-inline">

                                <input type="checkbox" class="custom-control-input" id="confirmation" name="confirmation" value="1" disabled {{ $confirmationCheck }}>
                                <label class="custom-control-label" for="confirmation">Przybycie</label>

                            </div>


                            <div class="custom-control custom-checkbox form-check-inline">

                                <input type="checkbox" class="custom-control-input" id="special_diet" name="special_diet" value="1" disabled {{ $special_dietCheck }}>
                                <label class="custom-control-label" for="special_diet">Dieta</label>

                            </div>

                            <div class="custom-control custom-checkbox form-check-inline">

                                <input type="checkbox" class="custom-control-input" id="accommodation" name="accommodation" value="1" disabled {{ $accommodationCheck }}>
                                <label class="custom-control-label" for="accommodation">Nocleg</label>

                            </div>

                            <div class="custom-control custom-checkbox form-check-inline">

                                <input type="checkbox" class="custom-control-input " id="transport" name="transport" value="1" disabled {{ $transportCheck }}>
                                <label class="custom-control-label" for="transport">Transport</label>

                            </div>

                        </div>

                        <div class="form-row pt-2">

                            <div class="form-group col-md-12">

                                <strong>Notatka:</strong>

                                <textarea disabled class="form-control bg-white" id="note" name="note" rows="2"> {{ $guest->note }} </textarea>

                            </div>

                        </div>

                </div>
            </div>
        </div>
    </div></div>


@endsection