<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="{{ mix('css/app.css') }}">

    <meta charset="utf-8">


</head>
<body class="bg-white">

    <div id="app" class="pb-5">

        <div>

            <nav class="navbar navbar-expand-md navbar-light navbar-laravel">

                <div class="container">

                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">

                        <span class="navbar-toggler-icon"></span>

                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <!-- Left Side Of Navbar -->
                        <ul class="navbar-nav mr-auto">

                        </ul>
                        <!-- Right Side Of Navbar -->
                        <ul class="navbar-nav ml-auto">
                            <!-- Authentication Links -->
                            @guest
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">Logowanie</a>
                                </li>

                                @if (Route::has('register'))
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('register') }}">Rejestracja</a>
                                    </li>
                                @endif

                            @else
                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>

                                        {{ Auth::user()->name }} <span class="caret"></span>

                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            Wyloguj się
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>

                                    </div>

                                </li>
                            @endguest

                        </ul>

                    </div>

                </div>

            </nav>
        </div>

        <div>

            <header class="masthead header-bg">

                <div class="container h-100 pt-5 pb-4">
                    <div class="row h-100 align-items-center">
                        <div class="col-12 text-center">
                            <h1 class="font-weight-light">Let'sWed</h1>
                            <p class="lead ">Zaplanuj swój ślub.</p>
                        </div>
                    </div>
                </div>

            </header>

        </div>

        @auth
        <div class="pt-3 d-flex justify-content-center">

            <div class="col-md-10">

                <nav class="navbar navbar-expand-md navbar-light bg-white border-top border-bottom navbar-laravel">

                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample10" aria-controls="navbarsExample10" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse justify-content-md-center" id="navbarsExample10">

                        <ul class="navbar-nav">

                            <li class="nav-item d-flex align-items-baseline {{ (request()->segment(1) == 'wedding') ? 'active' : '' }}">

                                <a class="nav-link" href="{{ route ('wedding.index') }}"><i class="fas fa-heart"> </i> Wesele</a>

                            </li>

                            <li class="nav-item d-flex align-items-baseline  {{ (request()->segment(1) == 'guest') ? 'active' : '' }}">

                                <a class="nav-link" href="{{ route('guest.index') }}"><i class="fas fa-users"></i> Goście</a>

                            </li>

                            <li class="nav-item d-flex align-items-baseline">

                                <a class="nav-link" href="#"><i class="fas fa-chair"> </i> Stoły</a>

                            </li>

                            <li class="nav-item d-flex align-items-baseline {{ (request()->segment(1) == 'task') ? 'active' : '' }}">

                                <a class="nav-link" href="{{ route('task.index') }}"><i class="far fa-list-alt"> </i> Zadania</a>

                            </li>

                            <li class="nav-item d-flex align-items-baseline {{ (request()->segment(1) == 'vendor') ? 'active' : '' }}">

                                <a class="nav-link" href="{{ route('vendor.index') }}"><i class="far fa-address-book"> </i> Usługodawcy</a>

                            </li>

                            <li class="nav-item d-flex align-items-baseline {{ (request()->segment(1) == 'cost') ? 'active' : '' }}">

                                <a class="nav-link" href="{{ route('cost.index') }}"><i class="fas fa-dollar-sign"> </i> Budżet</a>

                            </li>

                        </ul>
                    </div>

                </nav>
            </div>
        </div>
        @endauth

        <main class="py-4">
            @yield('content')
        </main>

        <footer class="page-footer font-small">

            <div class="footer-copyright text-center py-3">© 2019 Copyright: Let'sWed
            </div>

        </footer>

    </div>
</body>

</html>
