@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="row">
            <div class="pb-3">

                <a class="btn btn-outline-secondary" href="{{ route('vendor.index') }}"><i
                            class="fas fa-arrow-left"></i> Powrót</a>

            </div>
        </div>

        @if (count($errors) > 0)

            <div class="alert alert-danger">

                <strong>Ups!</strong> Wprowadzone dane są niepoprawne.<br><br>

                <ul>

                    @foreach ($errors->all() as $error)

                        <li>{{ $error }}</li>

                    @endforeach

                </ul>

            </div>

        @endif

        <div class="row">
            <div class="col-md-3 ">

                <h5>Kategorie: </h5>


                <ul class="list-group list-group-flush">
                    {!! Form::open(array('route' => 'vendor_category.store','method'=>'POST')) !!}
                    <li class="list-group-item d-flex justify-content-between align-content-center">
                        {!! Form::text('name', null, array('placeholder' => 'Dodaj kategorie','class' => 'form-control form-control-sm')) !!}
                        <div class="pl-1">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="fas fa-plus"
                                                                                    style="color: white"></i></button>
                        </div>
                    </li>

                    {!! Form::close() !!}
                    @foreach($vendor_categories as $category)
                        <li class="list-group-item d-flex justify-content-between align-content-center">{{ $category ->name }}

                            {!! Form::open(['method' => 'DELETE','route' => ['vendor_category.destroy', $category->id],'style'=>'display:inline']) !!}
                            {!! Form::button('<i class="fas fa-trash "></i>', ['type' => 'submit', 'class' => 'btn btn-link btn-sm']) !!}
                            {!! Form::close() !!}
                        </li>
                    @endforeach

                </ul>

            </div>

            <div class="col-md-9">

                <div align="center">
                    <div class="card w-75">

                        <div class="card-header text-center bg-light">

                            <h3> Dodaj usługodawcę </h3>

                        </div>

                        <div class="card-body">

                            {!! Form::open(array('route' => 'vendor.store','method'=>'POST')) !!}

                            <div class="form-row d-flex align-items-baseline">

                                <div class="form-group col-md-8">

                                    <strong>Firma:</strong>

                                    {!! Form::text('name', null, array('placeholder' => 'Nazwa firmy','class' => 'form-control')) !!}

                                </div>

                                <div class="form-group col-md-4">
                                    <strong>Wybierz kategorie: </strong>
                                    <select name="vendor_category_id" class="custom-select">
                                        @foreach($vendor_categories as $category)
                                            <option value="{{ $category -> id }}">{{ $category -> name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                            </div>

                            <div class="form-row">

                                <div class="form-group col-md-6">

                                    <strong>Email:</strong>

                                    {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}

                                </div>

                                <div class="form-group col-md-6">

                                    <strong>Telefon:</strong>

                                    {!! Form::text('phone_number', null, array('placeholder' => 'Telefon','class' => 'form-control')) !!}

                                </div>

                            </div>

                            <div class="form-row">

                                <div class="form-group col-md-5">

                                    <strong>Adres:</strong>

                                    {!! Form::text('street_address', null, array('placeholder' => 'Adres','class' => 'form-control')) !!}

                                </div>

                                <div class="form-group col-md-3">

                                    <strong>Kod pocztowy:</strong>

                                    {!! Form::text('postal_code', null, array('placeholder' => 'Kod pocztowy','class' => 'form-control')) !!}

                                </div>

                                <div class="form-group col-md-4">

                                    <strong>Miejscowość:</strong>

                                    {!! Form::text('city', null, array('placeholder' => 'Miejscowość','class' => 'form-control')) !!}

                                </div>

                            </div>

                            <div class="col-md-12 text-center">

                                <button type="submit" class="btn btn-primary">Zapisz</button>

                            </div>

                            {!! Form::close() !!}</div>
                    </div>
                </div>


            </div>
        </div>

    </div>


@endsection

