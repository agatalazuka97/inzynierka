@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="row justify-content-center">

            <div class="col-md-8">

                <div class="pb-3">

                    <a class="btn btn-outline-secondary" href="{{ route('vendor.index') }}"><i class="fas fa-arrow-left"></i> Powrót</a>

                </div>

                <div class="card">

                    <div class="card-header text-center bg-light">

                        <h3> Edytuj usługodawcę </h3>

                    </div>

                    <div class="card-body">

                        {!! Form::model($vendor, ['method' => 'PATCH','route' => ['vendor.update', $vendor->id]]) !!}

                        <div class="form-row d-flex align-items-baseline">

                            <div class="form-group col-md-8">

                                <strong>Firma:</strong>

                                {!! Form::text('name', null, array('placeholder' => 'Nazwa firmy','class' => 'form-control')) !!}

                            </div>

                            <div class="form-group col-md-4">
                                <strong>Wybierz kategorie: </strong>
                                <select name="vendor_category_id" class="custom-select">
                                    @foreach($vendor_categories as $category)
                                        <option {{ $selected_category == $category -> id ? 'selected' : '' }} value="{{ $category -> id }}">
                                            {{ $category -> name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                        </div>

                        <div class="form-row">

                            <div class="form-group col-md-6">

                                <strong>Email:</strong>

                                {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}

                            </div>

                            <div class="form-group col-md-6">

                                <strong>Telefon:</strong>

                                {!! Form::text('phone_number', null, array('placeholder' => 'Telefon','class' => 'form-control')) !!}

                            </div>

                        </div>

                        <div class="form-row">

                            <div class="form-group col-md-5">

                                <strong>Adres:</strong>

                                {!! Form::text('street_address', null, array('placeholder' => 'Adres','class' => 'form-control')) !!}

                            </div>

                            <div class="form-group col-md-3">

                                <strong>Kod pocztowy:</strong>

                                {!! Form::text('postal_code', null, array('placeholder' => 'Kod pocztowy','class' => 'form-control')) !!}

                            </div>

                            <div class="form-group col-md-4">

                                <strong>Miejscowość:</strong>

                                {!! Form::text('city', null, array('placeholder' => 'Miejscowość','class' => 'form-control')) !!}

                            </div>

                        </div>

                        <div class="col-md-12 text-center">

                            <button type="submit" class="btn btn-primary">Edytuj</button>

                        </div>

                        {!! Form::close() !!}</div>
                </div>
            </div>


        </div>
    </div>

@endsection