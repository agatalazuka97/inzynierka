@extends('layouts.app')

@section('content')
    <div class='container'>


        <div class="row">
            <div class="col-md-3">
                <h5>Kategorie: </h5>

                @foreach($vendor_info as $info)
                    <ul class="list-group list-group-flush">

                        <a href="/vendor/?category={{ $info -> category }}"
                           class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">{{ $info -> category }}
                            <span class="badge badge-primary badge-pill">{{ $info -> categories_count }}</span>
                        </a>
                    </ul>
                @endforeach


                <div class="pt-1">
                    <a href="/vendor">Wyczyść filtry</a>
                </div>
            </div>

            <div class="col-9">

                <div class="row justify-content-center pb-3">

                    <a class="btn btn-primary btn-lg cl-white" href="{{ route('vendor.create') }}"><i
                                class="fas fa-plus" style="color: white"></i> Dodaj usługodawcę</a>

                </div>

                <div class="row justify-content-center">
                    @foreach($vendors as $vendor)

                        <div class="col-md-4 col-12 pb-4">

                            <div class="card">

                                <div class="card-header card-vendor-background">

                                    <div class="text-center"><i class="{{ $vendor -> icon }} fa-2x"> </i></div>

                                    <h4 class="card-title text-center">{{ $vendor -> category }}</h4>

                                </div>

                                <div class="card-body">


                                    <h6 class="card-subtitle mb-2"><strong>Firma: </strong>{{ $vendor -> name }}</h6>

                                    <p class="card-text"><i
                                                class="fas fa-map-marker-alt"></i> {{ $vendor -> street_address }} <br>

                                        {{$vendor->postal_code}} {{ $vendor ->city }} </p>

                                    <p><i class="fas fa-phone"></i> {{ $vendor -> phone_number }}</p>

                                    <p><i class="fas fa-at"></i> {{ $vendor -> email }}</p>

                                    <div class="d-flex justify-content-between">
                                        <a class="btn btn-sm btn-link" href="{{ route('vendor.edit',$vendor->id) }}">Edytuj</a>
                                        {!! Form::open(['method' => 'DELETE','route' => ['vendor.destroy', $vendor->id],'style'=>'display:inline']) !!}
                                        {!! Form::button('Usuń', ['type' => 'submit', 'class' => 'btn btn-sm btn-link']) !!}
                                        {!! Form::close() !!}
                                    </div>

                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection