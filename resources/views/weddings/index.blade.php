@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row justify-content-center">


            <div class="col-10 justify-content-center">

                <h1>{{ $user -> name }} & {{ $wedding -> partner_name }}</h1>
                <i class="fas fa-map-marker-alt fa-lg"></i> {{ $local -> city }}, {{ $local -> name }}

                <div class="d-flex align-items-baseline pt-3">
                    Do ślubu zostało: <h2 id="countdown" class="pl-1"></h2>
                </div>


            </div>

        </div>
    </div>

<script>
    var countDownDate = new Date("{{$wedding -> wedding_date}}").getTime();
    var x = setInterval(function() {

        var now = new Date().getTime();
        var distance = countDownDate - now;

        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        document.getElementById("countdown").innerHTML = days + "d " + hours + "h "
            + minutes + "m " + seconds + "s ";

        if (distance < 0) {
            clearInterval(x);
            document.getElementById("countdown").innerHTML = "EXPIRED";
        }
    }, 1000);
</script>

@endsection


