@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Rejestracja - Krok 2</div>

                    <div class="card-body">

                        {!! Form::open(array('route' => 'wedding.store','method'=>'POST')) !!}

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Data Twojego ślubu: </label>

                            <div class="col-md-6">

                                {!! Form::date('wedding_date', null, array('class' => 'form-control')) !!}



                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Imię Twojego partnera: </label>

                            <div class="col-md-6">

                                {!! Form::text('partner_name', null, array('placeholder' => 'Imię partnera','class' => 'form-control')) !!}

                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Wybierz lokal: </label>

                            <div class="col-md-6">

                                <select name='local_id'  class="custom-select form-control">
                                    @foreach($locals as $local)
                                        <option value="{{ $local -> id }}">{{ $local -> name }}</option>
                                    @endforeach
                                </select>

                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Planowany budżet: </label>

                            <div class="col-md-6">

                                {!! Form::text('budget', null, array('placeholder' => 'Budżet','class' => 'form-control')) !!}

                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">

                                <button type="submit" class="btn btn-primary">
                                    Zakończ rejestrację
                                </button>
                                <br /><br />
                                <a href="{{ route('wedding.index') }}">Pomiń</a>

                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>


@endsection