@extends('layouts.app')

@section('content')
    <div class='container'>


        <div class="row">
            <div class="col-md-3">
                <h5>Kategorie: </h5>

                @foreach($costs_info as $info)
                    <ul class="list-group">

                        <a href="/cost/?name={{ $info -> name }}"
                           class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">{{ $info -> name }}
                            <span class="">{{ $info -> sum_planned }}</span>
                        </a>
                    </ul>
                @endforeach


                <div class="pt-1">
                    <a href="/cost">Wyczyść filtry</a>
                </div>
            </div>

            <div class="col-9">

                <div class="row justify-content-center pb-3">

                    <a class="btn btn-primary btn-lg cl-white" href="{{ route('cost.create') }}"><i
                                class="fas fa-plus" style="color: white"></i> Dodaj koszt</a>

                </div>

                <div class="row justify-content-center">

                    <div class="col-12 col-md-5 " >{!!$chart->container() !!}</div>
                    <div class="col-12 col-md-5">{!!$chart_actual->container() !!}</div>

                </div>

                <div class="row justify-content-center">

                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>

                                <th class="text-center">Wydatek</th>

                                <th class="text-center">Koszt planowany</th>

                                <th class="text-center">Koszt faktyczny</th>

                                <th class="text-center">Opcje</th>

                            </tr>
                            </thead>
                            <tbody>

                            @foreach($costs as $cost_category => $cost_lists)
                                <tr>
                                    <th class="bg-light" colspan="4" style="border: 0">{{ $cost_category }}</th>
                                </tr>

                                @foreach($cost_lists as $cost)

                                <tr>

                                    <td>{{ $cost -> title }} </td>

                                    <td class="text-center">{{ $cost -> cost_planned }}</td>

                                    <td class="text-center">{{ $cost -> cost_actual }}</td>

                                    <td class="text-center"></td>

                                </tr>

                                @endforeach

                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset=utf-8></script>

    {!! $chart->script() !!}
    {!! $chart_actual->script() !!}

@endsection

